(function () {
    'use strict';

    angular
        .module('angular', [
            'ui.router'
        ])
        .config(Routing);

    Routing.inject = ['$stateProvider', '$urlRouterProvider'];
    function Routing ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                abstract: true,
                views: {
                    app: {
                        templateUrl: './templates/app.html',
                        controller: 'AppController',
                        controllerAs: 'app'
                    }
                }
            })
            .state('app.login', {
                url: '/login',
                views: {
                    main: {
                        templateUrl: './templates/login.html',
                        controller: 'LoginController',
                        controllerAs: 'login'
                    }
                }
            })
            .state('app.dashboard', {
                url: '/dashboard',
                views: {
                    main: {
                        templateUrl: './templates/dashboard.html',
                        controller: 'DashboardController',
                        controllerAs: 'dashboard'
                    }
                }
            });

        $urlRouterProvider.otherwise("/login");
    }
})();

