(function () {
    'use strict';

    angular
        .module('angular')
        .controller('AppController', AppController);

    AppController.$inject = ['$state', '$scope'];

    function AppController ($state, $scope) {
        var app = this;

        setState($state.$current.name);
        app.message = '';
        app.states = [
            {
                name: 'Login',
                sref: 'app.login'
            },{
                name: 'Dashboard',
                sref: 'app.dashboard'
            }
        ];
        
        $scope.$on('eventName', function(event, params){
            app.message = params.message;
        });
        
        function setState(name) {
            app.state = name;
        }
    }

})();
