(function () {
    'use strict';

    angular
        .module('angular')
        .controller('DashboardController', DashboardController);

    DashboardController.$inject = [];

    function DashboardController () {
        var dashboard = this;

        dashboard.message = 'Dashboard';
    }

})();
