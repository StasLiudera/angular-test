(function () {
    'use strict';

    angular
        .module('angular')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope'];

    function LoginController ($scope) {
        var login = this;

        login.showMessage = showMessage;

        function showMessage () {
            $scope.$emit('eventName', {message: 'Hello from Login'});
        }
    }

})();
